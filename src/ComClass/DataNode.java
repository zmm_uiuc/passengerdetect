package ComClass;

import libsvm.svm_node;

public class DataNode {
	public svm_node[] pars;
	public double label;
	public DataNode(svm_node[] pars,double label){
		this.pars=pars;
		this.label=label;
	}
}
