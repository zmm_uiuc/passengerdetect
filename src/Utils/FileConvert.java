package Utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

public class FileConvert {
	
	
	public static boolean Convert(String source, String destination){
		boolean isSucess = false;
		File sourcefile=new File(source);
		List<String> rs=CSVUtils.importCsv(sourcefile);
		rs.remove(0);
		String[] ss;
		String outs;
		List<String> newList=new ArrayList<String>();
		for(String s : rs){
			ss=s.split(","); 
			outs=ss[ss.length-1];
			outs=outs.equals("TRUE")?"T":"F";
			for(int i=0;i<ss.length-1;i++){
				outs=outs+" "+i+":"+ss[i];
			}
			newList.add(outs);
		}
		File destFile=new File(destination);
		OutputTxt(destFile,newList);
		 
		return isSucess;
		
	}

	public static boolean OutputTxt(File file, List<String> dataList) {
		boolean isSucess = false;

		FileOutputStream out = null;
		OutputStreamWriter osw = null;
		BufferedWriter bw = null;
		try {
			out = new FileOutputStream(file);
			osw = new OutputStreamWriter(out);
			bw = new BufferedWriter(osw);
			if (dataList != null && !dataList.isEmpty()) {
				for (String data : dataList) {
					bw.append(data).append("\r\n");
				}
			}
			isSucess = true;
		} catch (Exception e) {
			isSucess = false;
		} finally {
			if (bw != null) {
				try {
					bw.close();
					bw = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (osw != null) {
				try {
					osw.close();
					osw = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if (out != null) {
				try {
					out.close();
					out = null;
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		return isSucess;
	}

}
