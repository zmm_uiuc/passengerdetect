package Utils;
 
import ComClass.DataNode;
import libsvm.svm_node;

public class SVMHelper {
	public static DataNode ParseNode(String s) {
		String[] ss = s.split(",");
		svm_node[] datanode = new svm_node[ss.length - 1];
		for(int i=0;i<ss.length-1;i++){
			svm_node par=new svm_node();
			par.index=i;
			par.value=Double.parseDouble(ss[i]);
			datanode[i]=par;
		} 
		return new DataNode(datanode,ss[ss.length-1].equals("TRUE")?1:-1);
	}
}
