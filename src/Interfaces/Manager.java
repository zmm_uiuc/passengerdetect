package Interfaces;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import ComClass.DataNode;
import Utils.CSVUtils;
import Utils.SVMHelper;
import libsvm.*;

public class Manager {

	public static void main(String[] args) throws IOException {

		//test();
		String source = "C:/Users/zmm/Desktop/行程识别/data-beijing.csv";
		TestReal(0.8, source);
		// String destination="C:/Users/zmm/Desktop/行程识别/data - selected
		// longtrip.txt";
		// FileConvert.Convert(source,destination);

	}

	/**
	 * 
	 * @param testratio
	 *            define the data ratio that used to train the data, usually set
	 *            as 0.8
	 * @param trainFile
	 */
	public static void TestReal(double testratio, String trainFile) {
		List<String> dataset = CSVUtils.importCsv(new File(trainFile));
		// generate the training data set into the required data format
		dataset.remove(0);
		DataNode dn;
		int trainSize = (int) (dataset.size() * testratio);
		double[] lables = new double[trainSize];
		svm_node[][] svmtrainNodes = new svm_node[trainSize][];
		for (int i = 0; i < trainSize; i++) {
			dn = SVMHelper.ParseNode(dataset.get(i));
			lables[i] = dn.label;
			svmtrainNodes[i] = dn.pars;
		}
		svm_problem problem = new svm_problem();
		problem.l = 2; // 向量个数
		problem.x = svmtrainNodes; // 训练集向量表
		problem.y = lables; // 对应的lable数组

		// 定义svm_parameter对象
		svm_parameter param = new svm_parameter();
		param.svm_type = svm_parameter.NU_SVC;
		param.kernel_type = svm_parameter.RBF;
		param.cache_size = 100;
		param.eps = 0.00001;
		param.C = 1;

		// 训练SVM分类模型
		System.out.println(svm.svm_check_parameter(problem, param)); // 如果参数没有问题，则svm.svm_check_parameter()函数返回null,否则返回error描述。
		System.out.println("model training");
		svm_model model = svm.svm_train(problem, param); // svm.svm_train()训练出SVM分类模型
		// test each training data
		double testSize = dataset.size() - trainSize;
		double correct = 0;
		System.out.println("start testing");
		for (int i = trainSize; i < dataset.size(); i++) {
			dn = SVMHelper.ParseNode(dataset.get(i));
			if (svm.svm_predict(model, dn.pars) == dn.label) {
				correct++;
			}
		}
		System.out.println("testing finished, testSize:"+testSize+"  correct number:"+correct);
		System.out.println(correct / testSize);
	}

	public static void test() {
		// 定义训练集点a{10.0, 10.0} 和 点b{-10.0, -10.0}，对应lable为{1.0, -1.0}
		svm_node pa0 = new svm_node();
		pa0.index = 0;
		pa0.value = 10.0;
		svm_node pa1 = new svm_node();
		pa1.index = -1;
		pa1.value = 10.0;
		svm_node pb0 = new svm_node();
		pb0.index = 0;
		pb0.value = -10.0;
		svm_node pb1 = new svm_node();
		pb1.index = -1;
		pb1.value = -10.0;
		svm_node[] pa = { pa0, pa1 }; // 点a
		svm_node[] pb = { pb0, pb1 }; // 点b
		svm_node[][] datas = { pa, pb }; // 训练集的向量表
		double[] lables = { 1.0, -1.0 }; // a,b 对应的lable

		// 定义svm_problem对象
		svm_problem problem = new svm_problem();
		problem.l = 2; // 向量个数
		problem.x = datas; // 训练集向量表
		problem.y = lables; // 对应的lable数组

		// 定义svm_parameter对象
		svm_parameter param = new svm_parameter();
		param.svm_type = svm_parameter.C_SVC;
		param.kernel_type = svm_parameter.LINEAR;
		param.cache_size = 100;
		param.eps = 0.00001;
		param.C = 1;

		// 训练SVM分类模型
		System.out.println(svm.svm_check_parameter(problem, param)); // 如果参数没有问题，则svm.svm_check_parameter()函数返回null,否则返回error描述。
		svm_model model = svm.svm_train(problem, param); // svm.svm_train()训练出SVM分类模型

		// 定义测试数据点c
		svm_node pc0 = new svm_node();
		pc0.index = 0;
		pc0.value = 1.1;
		svm_node pc1 = new svm_node();
		pc1.index = -1;
		pc1.value = 2.0;
		svm_node[] pc = { pc0, pc1 };
		// 预测测试数据的lable
		// System.out.println(svm.svm_predict(model, pc));
		System.out.println(svm.svm_predict(model, pc));
	}

}
